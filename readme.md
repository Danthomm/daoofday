# Solar Discord Bot

A Discord bot for sect events, reminders!

## Table of Contents
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Commands](#commands)
- [Configuration](#configuration)
- [Contributing](#contributing)
<!-- - [License](#license) -->

## Features
- Feature 1: Command Information
    - What it does: The !info command displays a list of available commands, providing users with information about the available bot functionalities.
    - How it works: Users can use the !info command to get a quick overview of the available commands and their descriptions.
- Feature 2: Time in the Game
    - What it does: The !time command retrieves and displays the current time in the game, allowing users to stay informed about the in-game time.
    - How it works: Users can execute the !time command to receive the current game time as a response from the bot.
- Feature 3: Message Deletion (Administrator-Only)
    - What it does: The !purge command enables administrators to delete a specified number of messages in the current channel, helping to maintain a clean and organized chat.
    - How it works: Administrators can use the !purge [number_of_messages] command to remove the designated number of messages from the channel.
- Feature 4: User Profile and XP
    - What it does: The !profile command allows users to check their rank and experience points (XP) in the server, providing a sense of progression within the community.
    - How it works: Users can execute the !profile command to receive information about their level, XP, and the XP needed to reach the next level. It can also be used to check the profile of other users by specifying their usernames.
- Feature 5: Event Reminders
    - What it does: The !remind command displays a list of upcoming events, ensuring that users stay informed about scheduled server events and activities.
    - How it works: Users can use the !remind command to receive a list of events, including their scheduled times, to help them prepare for and participate in various server events.
- Feature 6: Automatic Daily Events Reminders
    - What it does: The function reminds users that checked the emoji, about daily in game events.
    - How it works: Users can press emoji to gain a role that will ping them 1 hour / 10 minutes before a event. The emoji is set on a constant message on specified channel.
- Feature 7: Automatic Cleanup on Reminders Channel:
    - What it does: Automatically deletes all the messages on annoncements channel exceot specified one.
    - How it works: Bot monitors the time and everyday at 7:50 EST time cleans up the channel specified by administrator. It ignores the constant message.


## Installation
1. Clone this repository.
2. Create a bot on the Discord Developer Portal and get your token.
3. Install the necessary Python dependencies with `pip install -r requirements.txt`.
4. Configure the bot with your token and other settings (see [Configuration](#configuration)).
5. Run the bot with `python main.py`.

## Usage
- To trigger a specific command, type `!command-name` in a Discord server where the bot is added.
- Explain how to use the bot's features.

## Commands
- `!info`: Display a list of available commands.
- `!time`: Get the current time in the game.
- `!purge [number_of_messages]`: Delete messages. (administrator only)
- `!profile [username]`: Check your rank and XP.
- `!remind`: Display the list of upcoming events.

## Configuration
To configure the bot, create a `.env` file and add the following variables:

TOKEN=your_bot_token_here

Make sure to replace `your_bot_token_here` with your actual bot token.

## Contributing
- Fork the repository.
- Create a new branch for your feature or bug fix.
- Submit a pull request with your changes.

## Bot Messages

Here is a list of text messages used by the bot:

- `@Announcements The Beast Invasion starts in 1 hour! Be ready to take part in!`
- `@Announcements The Beast Invasion starts in 10 minutes! Be ready to take part in!`
- `@Announcements we are gonna take part in The Demonbend Abyss after the first Beast invasion! Let's join it together for damage bonus and better rewards!`
- `@Announcements today we are gonna fight with the other sect for the rulership of the Realm. Get ready for The Sect Clash, and don't forget to have fun!`
- `@Announcements we are gonna take part in The Sect Meditation after the first Beast invasion! Let's join it together for higher exp rewards!`
- `@Announcements today we are gonna take part in the World Apex! Make sure you are ready for it!`
- `@Announcements get ready for the sect clash!! We are starting in 30 minutes! :D!")`
- `@Announcements get ready for the world apex!!!! We are starting in 30 minutes! :D!")`

## Event List

Here is the list of events for today:
- First Beast Invasion in *time*
- Second Beast Invasion in *time*
- Sect Meditation in *time*
- Demonbend Abyss in *time*
- Sect Clash in *time*
- World Apex in *time*

These are the messages that the bot can send to announce events and keep users informed about upcoming activities in your server.