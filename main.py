import nextcord
import os
from nextcord.ext import commands
from dotenv import load_dotenv
from global_channel_id import developer_id, pick_role_message_id, pick_role_channel_id

load_dotenv()
token = os.getenv("TOKEN")

intents = nextcord.Intents.all()
intents.members = True
intents.typing = False
intents.presences = False
bot = commands.Bot(command_prefix='!', intents=intents)
bot.is_updating = False
intents.reactions = True

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name} ({bot.user.id})')
    print('----------------------')

    await bot.change_presence(activity=nextcord.Activity(type=nextcord.ActivityType.playing, name="Overmortal"))

    is_bot_updating = False
    developer = bot.get_user(developer_id)

    # check for developer before sending command, set the command state to update or not

    if is_bot_updating:
        if developer:
            bot.is_updating = False
        else:
            bot.is_updating = True
    else:
        bot.is_updating = False

@bot.command()
async def send_message(ctx):
    channel = bot.get_channel(pick_role_channel_id)
    
    message_content = "React with ✅ to get pinged on discord with in-game Events Announcements!"
    message = await channel.send(message_content)

    # Add the reactions to the message
    await message.add_reaction("✅")
    await message.add_reaction("❌")

@bot.event
async def on_raw_reaction_add(payload):
    user_id = payload.user_id
    guild_id = payload.guild_id
    emoji_name = payload.emoji.name

    # Check if the reaction is from the specific message you want to target
    if payload.message_id == pick_role_message_id:
        guild = bot.get_guild(guild_id)
        member = guild.get_member(user_id)

        if emoji_name == '✅':
            role = nextcord.utils.get(guild.roles, name="Announcements")
            if role is not None:
                await member.add_roles(role)
        elif emoji_name == '❌':
            role = nextcord.utils.get(guild.roles, name="Announcements")
            if role is not None:
                await member.remove_roles(role)

# load commands
bot.load_extension('load_cogs')

bot.run(token)