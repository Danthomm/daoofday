import os
import nextcord
from nextcord.ext import commands

class LoadCogs(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    # Loads commands from ./cogs and it's subfolders
    @commands.Cog.listener()
    async def on_ready(self):
        for filename in os.listdir('./cogs'):
            if filename.endswith('.py'):
                self.bot.load_extension(f'cogs.{filename[:-3]}')

        for filename in os.listdir('./cogs/events_auto'):
            if filename.endswith('.py'):
                self.bot.load_extension(f'cogs.events_auto.{filename[:-3]}')

        for filename in os.listdir('./cogs/events_hand'):
            if filename.endswith('.py'):
                self.bot.load_extension(f'cogs.events_hand.{filename[:-3]}')

def setup(bot):
    bot.add_cog(LoadCogs(bot))