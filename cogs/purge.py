import nextcord
from nextcord.ext import commands

class PurgeCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="purge", help='Delete messages; !purge number')
    @commands.has_guild_permissions(administrator=True)
    async def purge_messages(self, ctx, amount: int = 0):
        if self.bot.is_updating:
            await ctx.send("The bot is currently updating. Please wait for further instructions.")
        else:
            if amount <= 0:
                response = "Please provide a valid number of messages to delete."
                await ctx.send(response)
                return

            # Delete the command message
            await ctx.message.delete()

            try:
                await ctx.channel.purge(limit=amount)
                await ctx.send(f'Deleted {amount} messages.')
            except nextcord.Forbidden:
                await ctx.send("I don't have the necessary permissions to delete messages.")
            except nextcord.HTTPException:
                await ctx.send("An error occurred while trying to delete messages.")
            
def setup(bot):
    bot.add_cog(PurgeCog(bot))