import nextcord
from nextcord.ext import commands
import json

class XPSystem(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.xp_data = self.load_xp_data()

    def load_xp_data(self):
        try:
            with open('xp.json', 'r') as f:
                return json.load(f)
        except FileNotFoundError:
            return {}

    def save_xp_data(self):
        with open('xp.json', 'w') as f:
            json.dump(self.xp_data, f)
            
    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.bot:
            return  # Ignore messages from other bots

        author_id = str(message.author.name)
        if author_id not in self.xp_data:
            self.xp_data[author_id] = 0

        self.xp_data[author_id] += 1  # Increase XP by 1 for each message
        self.save_xp_data()

        # Check if the user's level changed and update their roles
        await self.update_user_roles(message.author)

    async def update_user_roles(self, user):

        xp = self.xp_data.get(str(user.id), 0)
        level = xp // 1000

        guild = user.guild

        # Define the roles for different levels
        roles_to_assign = {
            20: "Inner Disciples",
            50: "Core Disciples",
            100: "Prime Disciples"
        }

        for xp_level, role_name in roles_to_assign.items():
            role = nextcord.utils.get(guild.roles, name=role_name)

            # Check if the bot has the "manage_roles" permission
            if not guild.me.guild_permissions.manage_roles:
                print(f"The bot doesn't have the 'manage_roles' permission. Cannot assign roles.")
                return

            if level >= xp_level and role:
                if role not in user.roles:
                    await user.add_roles(role)
            else:
                if role in user.roles:
                    await user.remove_roles(role)

    @commands.command(help='Check your rank')
    async def profile(self, ctx, member: nextcord.Member = None):
        if self.bot.is_updating:
            await ctx.send("The bot is currently updating. Please wait for further instructions.")
            return

        member = member or ctx.author
        author_id = str(member.name)
        xp = self.xp_data.get(author_id, 0)
        level = xp // 1000

        if level >= 999:
            level_text = "999"
            xp_text = f'**XP to Next Level: Maxed**'
        else:
            level_text = level
            next_level_xp = (level + 1) * 1000
            xp_text = f'**XP to Next Level**: {next_level_xp - xp}'

        default_thumbnail = None

        # Check if the member has an avatar URL
        if member.avatar:
            avatar_url = member.avatar.url
        else:
            with open('avatar.jpg', 'rb') as file:
                default_thumbnail = nextcord.File(file)

        title = f'☯ {member.display_name} Profile ☯'
        description = f'**Level**: {level_text}\n**XP**: {xp}\n{xp_text}'
        color = nextcord.Color(0xFF5733)  # You can choose a different color

        # Create an embed for the profile card
        embed = nextcord.Embed(
            title=title,
            description=description,
            color=color
        )

        embed.color = color
        embed.set_thumbnail(url=avatar_url) if member.avatar else embed.set_thumbnail(url="attachment://avatar.jpg")

        await ctx.send(embed=embed, file=default_thumbnail)

def setup(bot):
    bot.add_cog(XPSystem(bot))