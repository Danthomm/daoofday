import datetime
import pytz
import nextcord
from nextcord.ext import commands, tasks
from cogs.events_hand.sect_meditation_hand import SectMeditationHandCog
from cogs.events_hand.beast_invasion_hand import BeastInvasionHandCog
from cogs.events_hand.sect_clash_hand import SectClashHandCog
from cogs.events_hand.demonbend_abyss_hand import DemonbendAbyssHandCog
from cogs.events_hand.world_apex_hand import WorldApexHandCog
from global_channel_id import channel_id, message_id

class EventListCog(commands.Cog):
    def __init__(self, bot):
        
        self.bot = bot
        self.events = []
        self.sect_meditation_hand_cog = SectMeditationHandCog()
        self.beast_invasion_hand_cog = BeastInvasionHandCog()
        self.sect_clash_hand_cog = SectClashHandCog()
        self.demonbend_abyss_hand_cog = DemonbendAbyssHandCog()
        self.world_apex_hand_cog = WorldApexHandCog()

        # Add events
        self.events.extend(self.sect_meditation_hand_cog.events)
        self.events.extend(self.beast_invasion_hand_cog.events)
        self.events.extend(self.sect_clash_hand_cog.events)
        self.events.extend(self.demonbend_abyss_hand_cog.events)
        self.events.extend(self.world_apex_hand_cog.events)

        # Create a background task to update the message every 5 minutes
        self.update_event_list.start()

    async def update_event_list_message(self, event_list_str):
        channel = self.bot.get_channel(channel_id)
        if channel:
            message = await channel.fetch_message(message_id)
            if message:
                await message.edit(content=event_list_str)

    @tasks.loop(minutes=5)  # Update the message every 5 minutes
    async def update_event_list(self):
        est_timezone = pytz.timezone('US/Eastern')
        current_time = datetime.datetime.now(est_timezone)

        event_list = []

        for event in self.events:
            if "time" in event and event["time"] is not None:
                event_time = event["time"].astimezone(est_timezone)
                time_difference = event_time - current_time
                hours, remainder = divmod(time_difference.total_seconds(), 3600)
                minutes, _ = divmod(remainder, 60)

                if event_time > current_time:
                    formatted_time = f"in {int(hours)} hours {int(minutes)} minutes"
                else:
                    formatted_time = "is happening now"

                event_list.append(f"{event['name']} {formatted_time}")
            else:
                event_list.append(event["name"])

        if event_list:
            event_list_str = "\n".join(event_list)
            await self.update_event_list_message(f"Here is the list of events for today:\n{event_list_str}")
        else:
            await self.update_event_list_message("There are no events today.")

    @commands.command(name="remind", help='Get the times for in game events')
    async def remind_command(self, ctx):
        event_list = self.get_event_list()
        if event_list:
            event_list_str = "\n".join(event_list)
            await ctx.send(f"Here is the list of events for today:\n{event_list_str}")
        else:
            await ctx.send("There are no events today.")

    def get_event_list(self):
        est_timezone = pytz.timezone('US/Eastern')
        current_time = datetime.datetime.now(est_timezone)

        event_list = []

        for event in self.events:
            if "time" in event and event["time"] is not None:
                event_time = event["time"].astimezone(est_timezone)
                if event_time > current_time:
                    time_difference = event_time - current_time
                    hours, remainder = divmod(time_difference.total_seconds(), 3600)
                    minutes, _ = divmod(remainder, 60)
                    formatted_time = f"{int(hours)} hours {int(minutes)} minutes"
                    event_list.append(f"{event['name']} in {formatted_time}")
                else:
                    event_list.append(event["name"])
            else:
                event_list.append(event["name"])

        return event_list

def setup(bot):
    bot.add_cog(EventListCog(bot))