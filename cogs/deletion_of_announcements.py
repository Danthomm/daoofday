import nextcord
from nextcord.ext import commands
from nextcord.ext import tasks
import pytz
import datetime
from global_channel_id import message_id, channel_id

class MessageDeletionCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.channel_id = channel_id
        self.est_timezone = pytz.timezone('US/Eastern')
        self.hour_to_delete = 7
        self.minute_to_delete = 50
        self.message_to_keep_id = message_id
        self.delete_messages.start()

    @tasks.loop(seconds=60)
    async def delete_messages(self):
        now = datetime.datetime.now(self.est_timezone)
        if now.hour == self.hour_to_delete and now.minute == self.minute_to_delete:
            channel = self.bot.get_channel(self.channel_id)
            if channel:
                await self.delete_messages_except_one(channel)

    async def delete_messages_except_one(self, channel):
        async for message in channel.history(limit=10).filter(lambda m: not m.pinned and m.id != self.message_to_keep_id):
            await message.delete()

    @delete_messages.before_loop
    async def before_delete_messages(self):
        await self.bot.wait_until_ready()


def setup(bot):
    bot.add_cog(MessageDeletionCog(bot))