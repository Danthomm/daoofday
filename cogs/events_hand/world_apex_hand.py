import datetime
import pytz
from nextcord.ext import commands

class WorldApexHandCog(commands.Cog):
    def __init__(self):
        self.events = []
        self.world_apex_hand()

    def world_apex_hand(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        world_apex = now.replace(hour=15, minute=0, second=0, microsecond=0)
        if now.weekday() in [6]:  # Sunday (6)
            if now <= world_apex:
                self.events.append({"name": "World Apex", "time": world_apex.astimezone(pytz.UTC)})
            else:
                self.events.append({"name": "Thanks for taking part in World Apex! :D", "time": None})

def setup(bot):
    bot.add_cog(WorldApexHandCog())
