import datetime
import pytz
from nextcord.ext import commands

class BeastInvasionHandCog(commands.Cog):
    def __init__(self):
        self.events = []
        self.first_invasion_hand()
        self.second_invasion_hand()

    def first_invasion_hand(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        first_invasion_time = now.replace(hour=12, minute=0, second=0, microsecond=0)
        if now <= first_invasion_time:
            self.events.append({"name": "First Beast Invasion", "time": first_invasion_time.astimezone(pytz.UTC)})

    def second_invasion_hand(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        second_invasion_time = now.replace(hour=18, minute=0, second=0, microsecond=0)
        if now <= second_invasion_time:
            self.events.append({"name": "Second Beast Invasion", "time": second_invasion_time.astimezone(pytz.UTC)})

def setup(bot):
    bot.add_cog(BeastInvasionHandCog())
