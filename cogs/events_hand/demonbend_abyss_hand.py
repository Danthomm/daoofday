import datetime
import pytz
from nextcord.ext import commands

class DemonbendAbyssHandCog(commands.Cog):
    def __init__(self):
        self.events = []
        self.demonbend_abyss_hand()


    def demonbend_abyss_hand(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        demonbend_abyss = now.replace(hour=8, minute=0, second=0, microsecond=0)
        if now.weekday() in [0, 2, 4]:  # Monday (0), Wednesday (2), or Friday (4)
            if now <= demonbend_abyss:
                self.events.append({"name": "Demonbend Abyss", "time": demonbend_abyss.astimezone(pytz.UTC)})
            else:
                self.events.append({"name": "Demonbend Abyss is live all day.", "time": None})
            
def setup(bot):
    bot.add_cog(DemonbendAbyssHandCog())