import datetime
import pytz
from nextcord.ext import commands

class SectClashHandCog(commands.Cog):
    def __init__(self):
        self.events = []
        self.sect_clash_hand()

    def sect_clash_hand(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        sect_clash = now.replace(hour=15, minute=0, second=0, microsecond=0)
        if now.weekday() in [5]:  # Saturday (5)
            if now <= sect_clash:
                self.events.append({"name": "Sect Clash", "time": sect_clash.astimezone(pytz.UTC)})
            else:
                self.events.append({"name": "Thanks for taking part in Sect Clash! :D", "time": None})

def setup(bot):
    bot.add_cog(SectClashHandCog())