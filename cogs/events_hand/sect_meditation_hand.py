import datetime
import pytz
from nextcord.ext import commands

class SectMeditationHandCog(commands.Cog):
    def __init__(self):
        self.events = []
        self.sect_meditation_hand()

    def sect_meditation_hand(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        sect_meditation = now.replace(hour=8, minute=0, second=0, microsecond=0)
        if now.weekday() in [1, 3]:  # 1 is Tuesday, 3 is Thursday
            if now <= sect_meditation:
                self.events.append({"name": "Sect Meditation", "time": sect_meditation.astimezone(pytz.UTC)})
            else:
                self.events.append({"name": "Sect Meditation is live all day.", "time": None})

def setup(bot):
    bot.add_cog(SectMeditationHandCog())
