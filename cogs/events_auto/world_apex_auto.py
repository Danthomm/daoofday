import nextcord
from nextcord.ext import commands, tasks
import datetime
import pytz
import global_channel_id

class world_apex(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.world_apex_auto.start()

    @tasks.loop(minutes=1)
    async def world_apex_auto(self):
        now = datetime.datetime.now()
        if now.weekday() == 6:  # Sunday (6)
            est = pytz.timezone('US/Eastern')
            now = datetime.datetime.now(est)
            if (now.hour == 8 and now.minute == 0):
                channel = self.bot.get_channel(global_channel_id.channel_id)
                mention = nextcord.utils.get(channel.guild.roles, name='Announcements')
                await channel.send(f"{mention.mention} today we are gonna take part in the World Apex! Make sure you are ready for it!")

            if (now.hour == 14 and now.minute == 30):
                channel = self.bot.get_channel(global_channel_id.channel_id)
                mention = nextcord.utils.get(channel.guild.roles, name='Announcements')
                await channel.send(f"{mention.mention} get ready for the world apex!! We are starting in 30 minutes! :D!")

    @world_apex_auto.before_loop
    async def before_world_apex_auto(self):
        await self.bot.wait_until_ready()

def setup(bot):
    bot.add_cog(world_apex(bot))