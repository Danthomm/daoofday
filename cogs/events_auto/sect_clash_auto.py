import nextcord
from nextcord.ext import commands, tasks
import datetime
import pytz
import global_channel_id

class sect_clash(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.sect_clash_auto.start()

    @tasks.loop(minutes=1)
    async def sect_clash_auto(self):
        now = datetime.datetime.now()
        if now.weekday() == 5:  # Saturday (5)
            est = pytz.timezone('US/Eastern')
            now = datetime.datetime.now(est)
            if (now.hour == 8 and now.minute == 0):
                channel = self.bot.get_channel(global_channel_id.channel_id)
                mention = nextcord.utils.get(channel.guild.roles, name='Announcements')
                await channel.send(f"{mention.mention} today we are gonna fight with the other sect for the rulership of the Realm. Get ready for The Sect Clash, and don't forget to have fun!")

            if (now.hour == 14 and now.minute == 30):
                channel = self.bot.get_channel(global_channel_id.channel_id)
                mention = nextcord.utils.get(channel.guild.roles, name='Announcements')
                await channel.send(f"{mention.mention} get ready for the sect clash!! We are starting in 30 minutes! :D!")

    @sect_clash_auto.before_loop
    async def before_sect_clash_auto(self):
        await self.bot.wait_until_ready()

def setup(bot):
    bot.add_cog(sect_clash(bot))