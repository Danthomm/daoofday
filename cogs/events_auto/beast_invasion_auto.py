import nextcord
from nextcord.ext import commands, tasks
import datetime
import pytz
import global_channel_id
import discord


class beast_invasion(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.beast_invasion_1_first.start()
        self.beast_invasion_1_second.start()
    client = discord.Client()
    @tasks.loop(minutes=1)
    async def beast_invasion_1_first(self, ctx):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        if (now.hour == 11 and now.minute == 0) or (now.hour == 17 and now.minute == 0):
            for guild in self.bot.guilds:
                if guild.id == guild_id1:
                    channel = self.bot.get_channel(global_channel_id.channel_id)  # Replace with your channel ID
                    mention = discord.utils.get(guild.roles, name='Announcements')
                elif guild.id == guild_id2:
                    channel = self.bot.get_channel(global_channel_id2.channel_id)  # Replace with your channel ID
                    mention = discord.utils.get(guild.roles, name='events')

            await channel.send(f"{mention.mention} The Beast Invasion starts in 1 hour! Be ready to take part in!")

    @tasks.loop(minutes=1)
    async def beast_invasion_1_second(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        if (now.hour == 11 and now.minute == 50) or (now.hour == 17 and now.minute == 50):
            for guild in self.bot.guilds:
                if guild.id == global_channel_id.guild_id1:
                    channel = self.bot.get_channel(global_channel_id.channel_id)  # Replace with your channel ID
                    mention = discord.utils.get(guild.roles, name='Announcements')
                elif guild.id == global_channel_id.guild_id2:
                    channel = self.bot.get_channel(global_channel_id.channel_id2)  # Replace with your channel ID
                    mention = discord.utils.get(guild.roles, name='events')

            await channel.send(f"{mention.mention} The Beast Invasion starts in 10 minutes! Be ready to take part in!")


    @beast_invasion_1_first.before_loop
    async def before_beast_invasion_1_first(self):
        await self.bot.wait_until_ready()
        
    @beast_invasion_1_second.before_loop
    async def before_beast_invasion_1_second(self):
        await self.bot.wait_until_ready()

def setup(bot):
    bot.add_cog(beast_invasion(bot))
