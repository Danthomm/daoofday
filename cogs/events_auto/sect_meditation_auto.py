import nextcord
from nextcord.ext import commands, tasks
import datetime
import pytz
import global_channel_id

class sect_meditation(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.sect_meditation_auto.start()

    @tasks.loop(minutes=1)
    async def sect_meditation_auto(self):
        now = datetime.datetime.now()
        if now.weekday() == 1 or now.weekday() == 3:  # Tuesday (1) or Thursday (3)
            est = pytz.timezone('US/Eastern')
            now = datetime.datetime.now(est)
            if (now.hour == 8 and now.minute == 0) or (now.hour == 12 and now.minute == 5):
                channel = self.bot.get_channel(global_channel_id.channel_id)
                mention = nextcord.utils.get(channel.guild.roles, name='Announcements')
                await channel.send(f"{mention.mention} we are gonna take part in The Sect Meditation after the first Beast invasion! (depends on server you are on) Let's join it togheter for higher exp rewards!")
                
    @sect_meditation_auto.before_loop
    async def before_sect_meditation_auto(self):
        await self.bot.wait_until_ready()

def setup(bot):
    bot.add_cog(sect_meditation(bot))