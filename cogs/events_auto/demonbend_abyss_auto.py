import nextcord
from nextcord.ext import commands, tasks
import datetime
import pytz
import global_channel_id

class demonbend_abyss(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.demonbend_abyss_auto.start()

    @tasks.loop(minutes=1)
    async def demonbend_abyss_auto(self):
        est = pytz.timezone('US/Eastern')
        now = datetime.datetime.now(est)
        if now.weekday() == 0 or now.weekday() == 2 or now.weekday() == 4:  # Monday (0), Wednesday (2), or Friday (4)
            if (now.hour == 8 and now.minute == 0) or (now.hour == 12 and now.minute == 5):
                channel = self.bot.get_channel(global_channel_id.channel_id)
                mention = nextcord.utils.get(channel.guild.roles, name='Announcements')
                await channel.send(f"{mention.mention} we are gonna take part in The Demonbend Abyss after the first Beast invasion! (depends on server you are on) Let's join it togheter for damage bonus and better rewards!")

    @demonbend_abyss_auto.before_loop
    async def before_demonbend_abyss_auto(self):
        await self.bot.wait_until_ready()

def setup(bot):
    bot.add_cog(demonbend_abyss(bot))