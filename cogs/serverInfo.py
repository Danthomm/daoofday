from discord.ext import commands

class ServerInfo(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='serverInfo')
    async def server_info(self, ctx):
        guild_ids = [guild.id for guild in self.bot.guilds]
        current_guild_id = ctx.guild.id if ctx.guild else None

        response = f"All server IDs: {guild_ids}\nCurrent server ID: {current_guild_id}"
        await ctx.send(response)

def setup(bot):
    bot.add_cog(ServerInfo(bot))