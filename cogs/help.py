from nextcord.ext import commands
import nextcord

class Help(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name='info', aliases=['commands'])
    async def show_commands(self, ctx):
        if self.bot.is_updating:
            await ctx.send("The bot is currently updating. Please wait for further instructions.")
            return
        
        prefix = ctx.prefix

        # Create an embed for the help message
        embed = nextcord.Embed(
            title='Available Commands',
            description=f'Use `{prefix}command` to run a command.',
            color=nextcord.Color.green()
        )

        # Define the order in which you want to display cogs
        cog_order = ['Help', 'TimeCog', 'PurgeCog', 'EventListCog', 'XPSystem']

        for cog_name in cog_order:
            cog_instance = self.bot.get_cog(cog_name)
            
            command_list = []

            # Customize the cog name
            if cog_name == 'TimeCog':
                cog_name = 'Time'
            elif cog_name == 'PurgeCog':
                cog_name = 'Purge'
            elif cog_name == 'EventListCog':
                cog_name = 'Event List'
            elif cog_name == 'XPSystem':
                cog_name = 'Rank'
                
            for command in cog_instance.get_commands():
                # Exclude specific commands by name
                if cog_name != "PurgeCog" and cog_name != "Purge":
                    command_list.append(f'`{prefix}{command.name}` - {command.help or "Message with information about commands"}')

            if command_list:
                embed.add_field(name=cog_name, value='\n'.join(command_list), inline=False)

        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Help(bot))