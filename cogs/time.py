import nextcord
from nextcord.ext import commands
import datetime
import pytz

class TimeCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="time", help='Get the current time in game')
    async def get_time(self, ctx):
        if self.bot.is_updating:
            await ctx.send("The bot is currently updating. Please wait for further instructions.")
        else:
            est = pytz.timezone('US/Eastern')
            current_time = datetime.datetime.now(est).strftime('%Y-%m-%d %H:%M:%S')
            response = f'Current time in EST is: {current_time}'
            await ctx.send(response)

def setup(bot):
    bot.add_cog(TimeCog(bot))